/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace Cielo.Core {
	/// @brief A football team.
	public class Team : IEquatable<Team> {
		public ulong ID { get; protected set; }

		public string Name { get; set; }

		public string ShortName { get; set; }

		/// @brief TLA (Three-Letter Acronym)
		public string Acronym { get; set; }

		public Team(ulong id, string name, string shortName = "", string acronym = "") {
			ID = id;
			Name = name;
			ShortName = shortName;
			Acronym = acronym;
		}

		public bool Equals(Team other) {
			if (other == null) {
				throw new ArgumentNullException("Cannot equal null teams.", "other");
			}

			return this.ID == ((Team) other).ID;
		}
	}
}
