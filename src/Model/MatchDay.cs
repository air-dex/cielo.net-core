/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;

namespace Cielo.Core {
	public class MatchDay<T> where T: notnull {
		public T Day { get; protected set; }

		/// @brief Standings before matches
		public Standings standingsBefore;

		/// @brief Standings after matches
		public Standings StandingsAfter { get; protected set; }

		public HashSet<Match> Matches { get; protected set; }

		public MatchDay(T day, Standings initialStandings, HashSet<Match> matches) {
			CieloUtils.CheckMatchesIntegrity(matches, initialStandings);

			Day = day;
			Matches = matches;
			standingsBefore = initialStandings;
			StandingsAfter = null;
		}

		public bool AreStandingsComputed() {
			return StandingsAfter != null;
		}

		// TODO: Do better EloComputer with services
		public Standings ComputeNewStandings(EloComputer<T> elo) {
			StandingsAfter = elo.ComputeMatchDay(Matches, standingsBefore);
			return StandingsAfter;
		}
	}
}
