/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Cielo.Core {
	public class TeamFactory: IJsonFactory<Team> {
		public Team FromJson(JsonElement jsonRoot) {

			try {
				return new Team(
					id: jsonRoot.GetProperty("id").GetUInt64(),
					name: jsonRoot.GetProperty("name").GetString()
						?? throw CieloUtils.BuildNullFieldException("name"),
					shortName: jsonRoot.GetProperty("shortName").GetString()
						?? throw CieloUtils.BuildNullFieldException("shortName"),
					acronym: jsonRoot.GetProperty("tla").GetString()
						?? throw CieloUtils.BuildNullFieldException("tla")
				);
			}
			catch (KeyNotFoundException ex) {
				throw new CieloJsonException($"Team field not found: {ex.Message}");
			}
			catch (ObjectDisposedException ex) {
				throw new CieloJsonException($"JSON document related to the reply is disposed: {ex.Message}");
			}
			catch (InvalidOperationException ex) {
				throw new CieloJsonException($"A problem occured while retrieving teams list: {ex.Message}");
			}
		}
	}
}
