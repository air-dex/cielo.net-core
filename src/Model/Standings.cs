/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Linq;

namespace Cielo.Core {
	public class Standings {
		protected List<Ranking> rankings;

		public List<Ranking> Rankings {
			get {
				rankings.Sort();
				rankings.Reverse();
				return rankings;
			}
		}

		public HashSet<Team> Teams {
			get { return rankings.GetTeams(); }
		}
		
		public Standings(): this(new List<Ranking>()) {}
		
		public Standings(List<Ranking> rankings) {
			// All the teams in standings should be different
			int nbTeams = rankings.Select(ranking => ranking.Team).Distinct().Count();

			if (nbTeams != rankings.Count) {
				throw new CieloException("All theams in the init standings should be different");
			}

			this.rankings = rankings;
		}

		public static Standings InitFromTeams(HashSet<Team> teams) {
			Standings res = new Standings();
			res.rankings = teams.Select(team => new Ranking(team)).ToList();
			return res;
		}

		public Ranking GetRanking(Team team) {
			return rankings.Where(r => r.Team == team).Single();
		}

		protected int GetTeamIndex(Team team) {
			return rankings.IndexOf(GetRanking(team));
		}

		public int GetPosition(Team team) {
			return 1 + GetTeamIndex(team);
		}

		public bool IsInStandings(Team team) {
			return rankings.Any(ranking => ranking.Team == team);
		}

		public void Update(List<Ranking> newRankings) {
			/*
			 * Teams check:
			 * 1°) All teams in newRankings should be in this.rankings
			 * 2°) A team in newRankings should not be present at least twice
			 */

			HashSet<Team> nrTeams = newRankings.GetTeams();

			if (nrTeams.Count != newRankings.Count) {
				throw new CieloException("All teams in a ranking update should be present once.");
			}

			if (!nrTeams.IsSubsetOf(Teams)) {
				throw new CieloException("All teams in a ranking update should update a team in the standings.");
			}

			foreach (Ranking ranking in newRankings) {
				int teamIndex = GetTeamIndex(ranking.Team);

				if (teamIndex == -1) {
					throw new CieloException("The team should be in standings.");
				}

				rankings[teamIndex].UpdateWith(ranking);
			}
		}
	}
}
