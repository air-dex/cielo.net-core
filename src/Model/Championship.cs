/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Linq;

namespace Cielo.Core {
	public class Championship<T> where T: notnull {
		public string Name { get; set; }
		
		/// @brief Championship code, related to the API.
		public int Code { get; set; }

		public int Season { get; set; }

		public SortedList<T, HashSet<Match>> Matches { get; set; }

		public SortedList<T, MatchDay<T>> MatchDays { get; set; }

		public List<Team> Teams { get; set; }

		protected Standings initialStandings;

		protected T initialJourney;

		public delegate SortedList<T, HashSet<Match>> SortMatches(List<Match> matches);
		public delegate T GetInitialIndex(SortedList<T, HashSet<Match>> matches);

		public Championship(
			string name, int code, int season,
			List<Match> matches, List<Team> teams,
			SortMatches sortMatchesFunc, GetInitialIndex getInitialIndexFunc
		) {
			initialStandings = Standings.InitFromTeams(teams.ToHashSet());
			CieloUtils.CheckMatchesIntegrity(matches.ToHashSet(), initialStandings);

			Name = name;
			Code = code;
			Season = season;
			Teams = teams;

			Matches = sortMatchesFunc(matches);
			initialJourney = getInitialIndexFunc(Matches);
			MatchDays = null;
		}

		public bool IsChampionshipComputed() {
			return MatchDays != null;
		}

		public SortedList<T, MatchDay<T>> ComputeChampionship(EloComputer<T> elo) {
			MatchDays = elo.ComputeChampionship(Matches, initialStandings, initialJourney);
			return MatchDays;
		}
	}
}
