/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace Cielo.Core {
	/// @see http://eloratings.net/about
	public class EloComputer<T>: IEloComputer<T> where T: notnull {
		public IEloConfiguration Configuration { get; set; }

		public EloComputer(IEloConfiguration eloconf) {
			Configuration = eloconf;
		}

		protected static double ComputeAdjustingWeight(uint goalDiff) {
			switch (goalDiff) {
				case 0:
				case 1:
					return 1;
				
				case 2:
					return 1.5;

				case 3:
					return 1.75;

				default:
					return 1.75 + (goalDiff-3)/8;
			}
		}

		protected Tuple<double, double> ComputeDifferenceInRatings(MatchPoints points) {
			// Tuples: Item1 is home and Item2 is away.
			return new Tuple<double, double>(
				item1: points.Home - points.Away + Configuration.HomePenalty,
				item2: points.Away - points.Home
			);
		}

		protected static double ComputeWinExpectancy(double dr) {
			return 1/(1 + Math.Pow(10, -dr/400));
		}

		protected Tuple<double, double> ComputeWinExpectancies(MatchPoints points) {
			// Tuples: Item1 is home and Item2 is away.
			Tuple<double, double> dr = ComputeDifferenceInRatings(points);

			return new Tuple<double, double>(
				item1: ComputeWinExpectancy(dr.Item1),
				item2: ComputeWinExpectancy(dr.Item2)
			);
		}

		protected Tuple<double, double> ComputeWinResults(Match match) {
			// Tuples: Item1 is home and Item2 is away.
			if (match.HomeWins()) {
				return new Tuple<double, double>(
					item1: Configuration.WinResult,
					item2: Configuration.LossResult
				);
			}
			else if (match.IsDraw()) {
				return new Tuple<double, double>(
					item1: Configuration.DrawResult,
					item2: Configuration.DrawResult
				);
			}
			else if (match.AwayWins()) {
				return new Tuple<double, double>(
					item1: Configuration.LossResult,
					item2: Configuration.WinResult
				);
			}
			else {
				throw new CieloException("Unknown result for the match.");
			}
		}

		/// @return Points exchange
		public MatchPoints ComputeMatch(Match match, MatchPoints points) {
			double adjK = ComputeAdjustingWeight(match.GoalDifference);

			// Win results
			Tuple<double, double> w = ComputeWinResults(match);

			// Win expectancies
			Tuple<double, double> we = ComputeWinExpectancies(points);

			// Tuples: Item1 is home and Item2 is away.
			return new MatchPoints(
				home: adjK * Configuration.Weight * (w.Item1 - we.Item1),
				away: adjK * Configuration.Weight * (w.Item2 - we.Item2)
			);
		}

		protected void ComputeAMatch(Match match, Standings standings, ref List<Ranking> rankingDiff) {
			// Looking for team points
			MatchPoints pointsBefore = new MatchPoints(
				home: standings.GetRanking(match.Home.Team).Points,
				away: standings.GetRanking(match.Away.Team).Points
			);

			// Computing point exchange
			MatchPoints pointsDiff = this.ComputeMatch(match, pointsBefore);
			uint homeMatch = (uint) (match.HomeWins() ? 1 : 0);
			uint drawMatch = (uint) (match.IsDraw() ? 1 : 0);
			uint awayMatch = (uint) (match.AwayWins() ? 1 : 0);

			// Adding ranking diffs
			rankingDiff.Add(new Ranking(
				tm: match.Home,
				matches: 1,
				wins: homeMatch,
				draws: drawMatch,
				losses: awayMatch,
				points: pointsBefore.Home,
				goalsAgainst: match.Away.Goals
			));
			rankingDiff.Add(new Ranking(
				tm: match.Away,
				matches: 1,
				wins: awayMatch,
				draws: drawMatch,
				losses: homeMatch,
				points: pointsBefore.Away,
				goalsAgainst: match.Home.Goals
			));
		}

		public Standings ComputeMatchDay(HashSet<Match> matches, Standings standings) {
			// No matches, no changes
			if (matches.Count == 0) {
				return standings;
			}

			CieloUtils.CheckMatchesIntegrity(matches, standings);

			// Compute matches
			List<Ranking> rankingDiff = new List<Ranking>();

			foreach (var match in matches) {
				ComputeAMatch(match, standings, ref rankingDiff);
			}

			// Patch standings
			standings.Update(rankingDiff);
			
			return standings;
		}

		public SortedList<T, MatchDay<T>> ComputeChampionship(
			SortedList<T, HashSet<Match>> matches,
			Standings initialStandings,
			T initialIndex
		) {
			SortedList<T, MatchDay<T>> res = new SortedList<T, MatchDay<T>>();

			// Creating the first entry
			MatchDay<T> firstMD = new MatchDay<T>(initialIndex, initialStandings, new HashSet<Match>(){});
			firstMD.ComputeNewStandings(this);
			res.Add(initialIndex, firstMD);

			// Creating other entries
			if (matches.Count > 0) {
				Standings currentStandings = initialStandings;

				// Computing championship
				foreach (KeyValuePair<T, HashSet<Match>> matchSet in matches) {
					// Computing a match day
					MatchDay<T> md = new MatchDay<T>(
						matchSet.Key,
						currentStandings,
						matchSet.Value
					);
					currentStandings = md.ComputeNewStandings(this);
					res.Add(matchSet.Key, md);
				}
			}

			return res;
		}
	}
}
