/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

namespace Cielo.Core {
	public class TeamMatch {
		public Team Team { get; protected set; }
		public uint Goals { get; protected set; }
		public uint YellowCards { get; protected set; }
		public uint RedCards { get; protected set; }

		public TeamMatch(Team team, uint goals, uint yellowCards = 0, uint redCards = 0) {
			Team = team;
			Goals = goals;
			YellowCards = yellowCards;
			RedCards = redCards;
		}
	}
}
