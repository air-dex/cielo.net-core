/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace Cielo.Core {
	public class Match {
		public TeamMatch Home { get; protected set; }

		public TeamMatch Away { get; protected set; }

		public DateTime Date { get; protected set; }

		public int MatchDay { get; protected set; }

		public uint GoalDifference {
			get { return (uint) Math.Abs((int) Home.Goals - (int) Away.Goals); }
		}

		public Match(TeamMatch home, TeamMatch away, DateTime date, int matchDay) {
			// A team cannot play against itself.
			if (home.Team == away.Team) {
				throw new CieloException("A team cannot play against itself.");
			}

			Home = home;
			Away = away;
			Date = date;
			MatchDay = matchDay;
		}

		public bool HomeWins() {
			return Home.Goals > Away.Goals;
		}

		public bool IsDraw() {
			return Home.Goals == Away.Goals;
		}

		public bool AwayWins() {
			return Home.Goals < Away.Goals;
		}
	}
}
