/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

namespace Cielo.Core {
	/// @see http://eloratings.net/about
	public interface IEloConfiguration {
		/// @brief Weight constant for the tournament played. Aka "K".
		/// It is always the same there tournament so K is not a critical
		/// parameter for the ratings. However, it may be useful for cosmetic
		/// purposes, for a user-friendly display.
		double Weight { get; set; }

		/// @brief A Penalty for the team playing at home.
		double HomePenalty { get; set; }

		/// @brief Coefficient for a win.
		double WinResult { get; set; }

		/// @brief Coefficient for a draw.
		double DrawResult { get; set; }

		/// @brief Coefficient for a loss.
		double LossResult { get; set; }
	}
}
