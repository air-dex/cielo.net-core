/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Cielo.Core {
	public class MatchFactory: IJsonFactory<Match> {
		public Match FromJson(JsonElement jsonRoot) {

			try {
				JsonElement homeTeam = jsonRoot.GetProperty("homeTeam");
				JsonElement awayTeam = jsonRoot.GetProperty("awayTeam");
				JsonElement scoreElt = jsonRoot.GetProperty("score").GetProperty("fullTime");

				return new Match(
					home: new TeamMatch(
						team: new Team(
							id: homeTeam.GetProperty("id").GetUInt64(),
							name: homeTeam.GetProperty("name").GetString()
								?? throw CieloUtils.BuildNullFieldException("homeTeam.name")
						),
						goals: scoreElt.GetProperty("homeTeam").GetUInt32()
					),
					away: new TeamMatch(
						team: new Team(
							id: awayTeam.GetProperty("id").GetUInt64(),
							name: awayTeam.GetProperty("name").GetString()
								?? throw CieloUtils.BuildNullFieldException("awayTeam.name")
						),
						goals: scoreElt.GetProperty("awayTeam").GetUInt32()
					),
					date: jsonRoot.GetProperty("utcDate").GetDateTime().GetDate(),
					matchDay: jsonRoot.GetProperty("matchday").GetInt32()
				);
			}
			catch (KeyNotFoundException ex) {
				throw new CieloJsonException($"Team field not found: {ex.Message}");
			}
			catch (ObjectDisposedException ex) {
				throw new CieloJsonException($"JSON document related to the reply is disposed: {ex.Message}");
			}
			catch (FormatException ex) {
				throw new CieloJsonException($"A value cannot be formatted properly: {ex.Message}");
			}
			catch (InvalidOperationException ex) {
				throw new CieloJsonException($"A problem occured while retrieving teams list: {ex.Message}");
			}
		}
	}
}
