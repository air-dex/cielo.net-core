/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace Cielo.Core {
	/// @brief A ranking in standings
	public class Ranking: IComparable<Ranking>, IEquatable<Ranking> {
		public Team Team { get; protected set; }

		public uint Matches { get; protected set; }

		public uint Wins { get; protected set; }

		public uint Draws { get; protected set; }

		public uint Losses { get; protected set; }

		public double Points { get; protected set; }

		public uint GoalsFor { get; protected set; }

		public uint GoalsAgainst { get; protected set; }

		public int GoalsDifference {
			get { return (int) GoalsFor - (int) GoalsAgainst; }
		}

		public uint RedCards { get; protected set; }

		public uint YellowCards { get; protected set; }

		public uint FairPlayPoints {
			get { return 3*RedCards + 1*YellowCards; }
		}

		/* TODO: Improve if i want to choose a comparison method
		public delegate int CompareInternalImpl(Ranking that, Ranking ranking);

		protected CompareInternalImpl cii;//*/

		public Ranking(
			Team team,
			uint matches = 0,
			uint wins = 0, uint draws = 0, uint losses = 0,
			double points = 0,
			uint goalsFor = 0, uint goalsAgainst = 0,
			uint redCards = 0, uint yellowCards = 0
		) {
			Team = team;
			Matches = matches;
			Wins = wins;
			Draws = draws;
			Losses = losses;
			Points = points;
			GoalsFor = goalsFor;
			GoalsAgainst = goalsAgainst;
			RedCards = RedCards;
			YellowCards = YellowCards;
		}

		public Ranking(
			TeamMatch tm,
			uint matches = 0,
			uint wins = 0, uint draws = 0, uint losses = 0,
			double points = 0,
			uint goalsAgainst = 0
		) : this(
			team: tm.Team,
			matches: matches, wins: wins, draws: draws, losses: losses,
			points: points,
			goalsFor: tm.Goals, goalsAgainst: goalsAgainst,
			redCards: tm.RedCards, yellowCards: tm.YellowCards
		) {}

		/// @fn protected int CompareInternal(Ranking ranking);
		/// @brief Internal method to compare rankings.
		/// @param ranking The ranking to compare
		/// @return A postive integer if this > ranking, a negative one
		/// whether this < ranking, or 0 if this == ranking.
		protected virtual int CompareInternal(Ranking ranking) {
			int criteria;

			// 1°) The best has got more points.
			criteria = (int) Math.Truncate(this.Points - ranking.Points);
			if (criteria != 0) {
				return criteria;
			}

			// 2°) The best has got the best goal difference.
			criteria = this.GoalsDifference - ranking.GoalsDifference;
			if (criteria != 0) {
				return criteria;
			}

			// 3°) The best has scored more goals.
			criteria = (int) this.GoalsFor - (int) ranking.GoalsFor;
			if (criteria != 0) {
				return criteria;
			}

			// 4°) The best has won more often.
			criteria = (int) this.Wins - (int) ranking.Wins;
			if (criteria != 0) {
				return criteria;
			}

			// 5°) The best has more draws.
			criteria = (int) this.Draws - (int) ranking.Draws;
			if (criteria != 0) {
				return criteria;
			}

			// 6°) The best has got less losses.
			criteria = (int) this.Losses - (int) ranking.Losses;
			if (criteria != 0) {
				return -criteria;
			}

			// 7°) The best has got the best fair-play (less fair-play points).
			criteria = (int) this.FairPlayPoints - (int) ranking.FairPlayPoints;
			if (criteria != 0) {
				return -criteria;
			}

			return 0;
		}

		public int CompareTo (Ranking obj) {
			if (obj == null) {
				throw new ArgumentNullException("Cannot compare null rankings.","obj");
			}

			return this.CompareInternal((Ranking) obj);
		}

		public bool Equals(Ranking other) {
			if (other == null) {
				throw new ArgumentNullException("Cannot equal null rankings.","other");
			}

			return this.CompareInternal((Ranking) other) == 0;
		}

		/// @brief Updating a ranking with its evolution (after a match for example)
		/// @throws CieloException Throws if the team in not the same in ranking.
		public void UpdateWith(Ranking ranking) {
			if (this.Team != ranking.Team) {
				throw new CieloException("A ranking should be updated with a ranking of the same team.");
			}

			Matches += ranking.Matches;
			Wins += ranking.Wins;
			Draws += ranking.Draws;
			Losses += ranking.Losses;
			Points += ranking.Points;
			GoalsFor += ranking.GoalsFor;
			GoalsAgainst += ranking.GoalsAgainst;
			RedCards += ranking.RedCards;
			YellowCards += ranking.YellowCards;
		}
	}
}
