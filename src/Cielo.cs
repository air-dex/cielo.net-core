﻿/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using Cielo.Core.Connection;

namespace Cielo.Core {
	public static class CieloExtensions {
		public static HashSet<Team> GetTeams(this List<Ranking> rankings) {
			return rankings.Select(r => r.Team).ToHashSet();
		}

		public static string ToQueryString(this Dictionary<string, string> dico) {
			string[] entries = dico
				.Select(e => $"{HttpUtility.UrlEncode(e.Key)}={HttpUtility.UrlEncode(e.Value)}")
				.ToArray();
			return $"?{string.Join('&', entries)}";
		}

		public static void Configure(this HttpClient client, IConnectionConfiguration conf) {
			client.DefaultRequestHeaders.Add("X-Auth-Token", conf.APIKey);
			client.Timeout = conf.Timeout;
			client.BaseAddress = conf.BaseURL;
		}

		public static string GetHeader(this HttpResponseHeaders headers, string headerName) {
			try {
				return headers
					.Where(header => header.Key == headerName)
					.Select(header => header.Value)
					.Single().ToList()[0];
			}
			catch (ArgumentNullException e) {
				throw new CieloException($"Cannot retrieve the unique value for the {headerName} header: {e.Message}");
			}
			catch (InvalidOperationException e) {
				throw new CieloException($"Cannot retrieve the unique value for the {headerName} header: {e.Message}");
			}
		}

		/// @removes the "time" part of the date.
		public static DateTime GetDate(this DateTime date) {
			return new DateTime(
				year: date.Year, month: date.Month, day: date.Day,
				hour: 0, minute: 0, second: 0, millisecond: 0
			);
		}

		public static void AddCielo(this IServiceCollection services) {
			services.AddTransient<ICieloClient, CieloClient>();
			services.AddScoped<IConnectionConfiguration, ConnectionConfiguration>();
			services.AddTransient<IFDClient, FDClient>();
			services.AddScoped<IEloConfiguration, EloConfiguration>();
			services.AddScoped<IEloComputer<int>, EloComputer<int>>();
			services.AddScoped<IEloComputer<DateTime>, EloComputer<DateTime>>();
		}
    }

	public class CieloUtils {
		public static void CheckMatchesIntegrity(HashSet<Match> matches, Standings standings) {
			/*
			 * Checking arguments:
			 * 
			 * 1°) All playing teams should be in standings.
			 * 2°) A team cannot play more than 2 matches per match day.
			 */

			var homeTeams = matches.Select(match => match.Home.Team);
			var awayTeams = matches.Select(match => match.Away.Team);
			var allTeams = homeTeams.Concat(awayTeams);

			if (allTeams.Distinct().Count() != 2*matches.Count) {
				throw new CieloException("A team cannot play more than 2 matches per match day.");
			}

			if (!allTeams.ToHashSet().IsSubsetOf(standings.Teams)) {
				throw new CieloException("All playing teams should be present in standings.");
			}
		}

		public static SortedList<int, HashSet<Match>> SortMatchesByMatchDay(List<Match> matches) {
			return new SortedList<int, HashSet<Match>>(
				matches
					.GroupBy(match => match.MatchDay)
					.ToDictionary(mgr => mgr.Key, mgr => mgr.ToHashSet())
			);
		}

		public static SortedList<DateTime, HashSet<Match>> SortMatchesByDate(List<Match> matches) {
			return new SortedList<DateTime, HashSet<Match>>(
				matches
					.GroupBy(match => match.Date)
					.ToDictionary(mgr => mgr.Key, mgr => mgr.ToHashSet())
			);
		}

		public static int GetInitialMatchDay(SortedList<int, HashSet<Match>> matches) {
			return matches.Count == 0 ? 0 : (matches.First().Key -1) ;
		}

		public static DateTime GetInitialDate(SortedList<DateTime, HashSet<Match>> matches) {
			return matches.Count == 0 ? DateTime.Now : (matches.First().Key - TimeSpan.FromDays(1)) ;
		}

		public static CieloJsonException BuildNullFieldException(string field) {
			return new CieloJsonException($"Unexpected null string for '{field}' field.");
		}
	}
}
