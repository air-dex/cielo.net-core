/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Cielo.Core.Connection.Exceptions;

namespace Cielo.Core.Connection {
	public class FDClient: IFDClient {
		public HttpClient WebClient { get; set; }
		protected static readonly string COUNTER_RESET_HEADER = "X-RequestCounter-Reset";
		protected static readonly string REQUESTS_AVAILABLE_HEADER = "X-Requests-Available-Minute";

		public IConnectionConfiguration Configuration { get; set; }

		public FDClient(HttpClient client, IConnectionConfiguration configuration) {
			WebClient = client;
			Configuration = configuration;
		}

		public async Task<RawReply> AskAPIAsync(string path, Dictionary<string, string> queryArgs) {
			WebClient.Configure(Configuration);

			UriBuilder url = new UriBuilder(Configuration.BaseURL);
			url.Path += path;
			url.Query = queryArgs.ToQueryString();

			bool isOK;
			int httpCode;
			string httpReason;
			uint counterReset, requestsAvailable;
			JsonElement jsonRoot;

			using (HttpResponseMessage response = await WebClient.GetAsync(url.Uri)) {
				isOK = response.IsSuccessStatusCode;
				httpCode = (int) response.StatusCode;
				httpReason = response.ReasonPhrase ?? response.StatusCode.ToString();

				HttpResponseHeaders headers = response.Headers;
				
				try {
					counterReset = uint.Parse(headers.GetHeader(COUNTER_RESET_HEADER));
					requestsAvailable = uint.Parse(headers.GetHeader(REQUESTS_AVAILABLE_HEADER));
				}
				catch (CieloException cex) {
					throw new FailingRequestException(cex.Message);
				}
				catch (ArgumentNullException ex) {
					throw new FailingRequestException(ex.Message);
				}
				catch (ArgumentException ex) {
					throw new FailingRequestException(ex.Message);
				}
				catch (FormatException ex) {
					throw new FailingRequestException(ex.Message);
				}
				catch (OverflowException ex) {
					throw new FailingRequestException(ex.Message);
				}

				using (JsonDocument json = await response.Content.ReadFromJsonAsync<JsonDocument>()) {
					if (json == null) {
						throw new FailingRequestException("Unexpected null JSON document as reply.");
					}

					jsonRoot = json.RootElement.Clone();
				}
			}

			return new RawReply(
				isOK: isOK,
				code: httpCode, reason: httpReason,
				counterReset: counterReset, requestsAvailable: requestsAvailable,
				json: jsonRoot
			);
		}
	}
}
