/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;

namespace Cielo.Core.Connection {
	public class ConnectionConfiguration: IConnectionConfiguration {
		public string APIKey { get; set; }
		public TimeSpan Timeout { get; set; }
		public Uri BaseURL { get; set; }

		public ConnectionConfiguration(string apiKey, TimeSpan timeout, Uri baseURL) {
			APIKey = apiKey;
			Timeout = timeout;
			BaseURL = baseURL;
		}

		/// @param timeout Timeout in milliseconds.
		public ConnectionConfiguration(string apiKey, double timeout, string baseURL) : this(
			apiKey: apiKey,
			timeout: TimeSpan.FromMilliseconds(timeout),
			baseURL: new Uri(baseURL)
		) {}

		// todo
	}
}
