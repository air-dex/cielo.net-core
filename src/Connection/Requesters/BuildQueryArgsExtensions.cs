/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using Cielo.Core.Connection.Exceptions;

namespace Cielo.Core.Connection.Requesters {
	public static class BuildQueryArgsExtensions {
		static readonly string FD_DATE_FORMAT = "YYYY-MM-DD";

		static FailingRequestException ThrowNull(string nullField) {
			return new FailingRequestException($"{nullField} value should not be null");
		}

		static void ThrowTechnicalQueryArgsIssue(Exception ex) {
			throw new FailingRequestException($"An issue occured while formatting query arguments: {ex.Message}");
		}

		static void addInt(
			this Dictionary<string, string> queryArgs, string fieldName, int? i
		) {
			try {
				if (i != null) {
					queryArgs.Add(fieldName, i.ToString()?? throw ThrowNull(fieldName));
				}
			}
			catch (ArgumentNullException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
			catch (ArgumentException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
			
		}

		static void addString(this Dictionary<string, string> queryArgs, string fieldName, string s) {
			try {
				if (s != null) {
					queryArgs.Add(fieldName, s);
				}
			}
			catch (ArgumentNullException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
			catch (ArgumentException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
		}

		static void addEnum<T>(this Dictionary<string, string> queryArgs, string fieldName, T? enval)
			where T: struct, IConvertible
		{
			try {
				if (enval != null) {
					queryArgs.Add(fieldName, enval.ToString() ?? throw ThrowNull(fieldName));
				}
			}
			catch (FormatException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
			catch (ArgumentOutOfRangeException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
		}

		static void addDate(this Dictionary<string, string> queryArgs, string fieldName, DateTime? date) {
			try {
				if (date != null) {
					DateTime d = date ?? throw new FailingRequestException("Totally unexpected null date");
					queryArgs.Add(fieldName, d.ToString(FD_DATE_FORMAT));
				}
			}
			catch (FormatException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
			catch (ArgumentOutOfRangeException ex) {
				ThrowTechnicalQueryArgsIssue(ex);
			}
		}

		public static void addSeason(
			this Dictionary<string, string> queryArgs, int? season
		) {
			addInt(queryArgs, "season", season);
		}

		public static void addMatchDay(
			this Dictionary<string, string> queryArgs, int? matchday
		) {
			addInt(queryArgs, "matchday", matchday);
		}

		public static void addStage(this Dictionary<string, string> queryArgs, string stage) {
			addString(queryArgs, "stage", stage);
		}

		public static void addGroup(this Dictionary<string, string> queryArgs, string group) {
			addString(queryArgs, "group", group);
		}

		public static void addDateFrom(this Dictionary<string, string> queryArgs, DateTime? dateFrom) {
			addDate(queryArgs, "dateFrom", dateFrom);
		}

		public static void addDateTo(this Dictionary<string, string> queryArgs, DateTime? dateTo) {
			addDate(queryArgs, "dateTo", dateTo);
		}

		public static void addStatus(this Dictionary<string, string> queryArgs, MatchStatus? status) {
			addEnum(queryArgs, "status", status);
		}
	}
}
