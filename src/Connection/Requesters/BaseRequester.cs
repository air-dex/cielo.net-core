/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Cielo.Core.Connection.Exceptions;

namespace Cielo.Core.Connection.Requesters {
	public abstract class BaseRequester<T>: IRequester<T> {
		public IFDClient FootApiClient { get; set; }

		public string Path { get; protected set; }

		public BaseRequester(IFDClient client, string path) {
			FootApiClient = client;
			Path = path;
		}

		public virtual Dictionary<string, string> BuildQueryArgs() {
			return new Dictionary<string, string>();
		}

		/// @throws FailingRequestException Thrown in case of technical issues.
		/// @throws CieloJsonException Thrown in case of JSON-related technical issues.
		/// @throws WrongReplyException Thrown if the request did not succeed.
		public async virtual Task<T> executeRequest() {
			Dictionary<string, string> queryArgs = BuildQueryArgs();

			try {
				RawReply reply = await FootApiClient.AskAPIAsync(Path, queryArgs);

				if (reply.IsOK) {
					// Building right result
					return BuildResult(reply);
				}
				else {
					// Building wrong reply
					int errorCode = reply.HttpCode;
					string reason = reply.HttpReason;
					string errorMessage = RetrieveErrorMessage(
						reply.Json ?? throw new FailingRequestException("Unexpected null raw reply.")
					);
					WrongReply wrong = errorCode == 429 ?
						  new ExcedeedQuotaReply(
							  httpCode: errorCode,
							  httpReason: reason,
							  errorMessage: errorMessage,
							  counterReset: reply.CounterReset,
							  requestsAvailable: reply.RequestsAvailable
						  )
						: new WrongReply(
							httpCode: errorCode,
							httpReason: reason,
							errorMessage: errorMessage
						);
					throw new WrongReplyException(wrong);
				}
			}
			catch (TimeoutException e) {
				throw new FailingRequestException($"Request has reached timeout: {e.Message}");
			}
			catch (ArgumentNullException e) {
				throw new FailingRequestException($"An unexpected null object took place in the request: {e.Message}");
			}
			catch (InvalidOperationException e) {
				throw new FailingRequestException($"An issue occured during the request: {e.Message}");
			}
			catch (HttpRequestException e) {
				throw new FailingRequestException($"Connection got issues during request: {e.Message}");
			}			
		}

		protected string RetrieveErrorMessage(JsonElement json) {
			try {
				return json.GetProperty("message").GetString()
					?? throw new CieloJsonException("Unexpected null JSON item.");
			}
			catch (ObjectDisposedException e) {
				throw new CieloJsonException(
					$"Cannot use JSON element of a disposed JSON document: {e.Message}"
				);
			}
			catch (InvalidOperationException e) {
				throw new CieloJsonException($"Wrong operation while retrieveing error message: {e.Message}");
			}
			catch (KeyNotFoundException e) {
				throw new CieloJsonException($"Cannot find a value for the field: {e.Message}");
			}
		}

		public virtual T BuildResult(RawReply reply) {
			JsonElement json = reply.Json
				?? throw new FailingRequestException("JSON data should not be null while building a right answer.");
			return BuildResultFromJSON(json);
		}

		public virtual T BuildResultFromJSON(JsonElement json) {
			throw new CieloException("This method should be overriden.");
		}
	}
}
