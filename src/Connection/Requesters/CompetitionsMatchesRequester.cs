/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Cielo.Core.Connection.Requesters {
	public class CompetitionsMatchesRequester: BaseRequester<List<Match>> {
		public DateTime? DateFrom { get; protected set; }
		public DateTime? DateTo { get; protected set; }
		public MatchStatus? Status { get; protected set; }
		public int? MatchDay { get; protected set; }
		public string Stage { get; protected set; }
		public string Group { get; protected set; }
		public int? Season { get; protected set; }

		public CompetitionsMatchesRequester(
			IFDClient client, CompetitionCode ccode,
			int? season, MatchStatus? status,
			DateTime? dateFrom, DateTime? dateTo, int? matchDay,
			string stage, string group
		): base(
			client: client,
			path: $"/competitions/{ccode.ToString()}/matches"
		) {
			DateFrom = dateFrom;
			DateTo = dateTo;
			Status = status;
			Stage = stage;
			MatchDay = matchDay;
			Group = group;
			Season = season;
		}

		public override Dictionary<string, string> BuildQueryArgs() {
			Dictionary<string, string> res = new Dictionary<string, string>();

			res.addDateFrom(DateFrom);
			res.addDateTo(DateTo);
			res.addStage(Stage);
			res.addStatus(Status);
			res.addMatchDay(MatchDay);
			res.addSeason(Season);
			res.addGroup(Group);

			return res;
		}

		public override List<Match> BuildResultFromJSON(JsonElement json) {
			try {
				List<Match> matches = new List<Match>();
				MatchFactory mf = new MatchFactory();

				JsonElement jsonMatches = json.GetProperty("matches");
				foreach(JsonElement elt in jsonMatches.EnumerateArray()) {
					matches.Add(mf.FromJson(elt));
				}

				return matches;
			}
			catch (KeyNotFoundException) {
				throw new CieloJsonException("Reply for competion's teams should have a 'teams' field.");
			}
			catch (ObjectDisposedException ex) {
				throw new CieloJsonException($"JSON document related to the reply is disposed: {ex.Message}");
			}
			catch (InvalidOperationException ex) {
				throw new CieloJsonException($"A problem occured while retrieving teams list: {ex.Message}");
			}
		}
	}
}
