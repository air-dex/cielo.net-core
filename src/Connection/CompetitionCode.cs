/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

namespace Cielo.Core.Connection {
	public enum CompetitionCode {
		/************
		 * TIER_ONE *
		 ************/

		/// @brief Brasileirão e Série A (Brazil)
		BSA,
		
		/// @brief Premier League (England)
		PL,
		
		/// @brief Championship (England)
		ELC,
		
		/// @brief UEFA Champions League (Europe)
		CL,
		
		/// @brief Euro (Europe)
		EC,
		
		/// @brief Ligue 1 (France)
		FL1,
		
		/// @brief Bundesliga (Germany)
		BL1,
		
		/// @brief Serie A (Italy)
		SA,
		
		/// @brief Eredivisie (Netherlands)
		DED,
		
		/// @brief Primeira Liga (Portugal)
		PPL,
		
		/// @brief Primera División de España (Spain). Aka "La Liga".
		PD,
		
		/// @brief World Cup (World)
		WC,
		
		/************
		 * TIER_TWO *
		 ************/
		
		/// @brief Jupiler League (Belgium)
		BJL,
		
		/// @brief League One (England)
		EL1,
		
		/// @brief Europa League (Europe)
		UEFA,
		
		/// @brief German Cup (Germany)
		DFB,
		
		/// @brief 2. Bundesliga (Germany)
		BL2,
		
		/// @brief Serie B (Italy)
		SB,
		
		/// @brief Segunda Division (Spain)
		SD,
		
		/**************
		 * TIER_THREE *
		 **************/

		/// @brief League Two (England)
		EL2,

		/// @brief Ligue 2 (France)
		FL2,
		
		/// @brief 3. Bundesliga (Germany)
		BL3,
		
		/*************
		 * TIER_FOUR *
		 *************/
		
		/// @brief National League (England)
		NAT
	}
}
