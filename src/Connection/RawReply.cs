/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System.Text.Json;

namespace Cielo.Core.Connection {
	public class RawReply {
		/// @brief HTTP return code
		public int HttpCode { get; protected set; }

		/// @brief HTTP reason related to code
		public string HttpReason { get; protected set; } = null!;

		/// @brief Do
		public bool IsOK { get; protected set; }

		/// @brief Seconds befre resetting the request counter
		public uint CounterReset { get; protected set; }

		/// @brief Number of requests available befor being blocked (with the 429 code).
		public uint RequestsAvailable { get; protected set; }

		/// @brief Json reply
		public JsonElement? Json { get; protected set; }

		public RawReply(bool isOK,
			int code = 0, string reason = "",
			uint counterReset = 0, uint requestsAvailable = 0,
			JsonElement? json = null
		){
			IsOK = isOK;
			HttpCode = code;
			HttpReason = reason;
			CounterReset = counterReset;
			RequestsAvailable = requestsAvailable;
			Json = json;
		}
	}
}
