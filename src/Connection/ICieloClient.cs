/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Cielo.Core.Connection.Requesters;

namespace Cielo.Core.Connection {
	/// @brief Front-end client for Football-data
	public interface ICieloClient {
		IFDClient FootApiClient { get; set; }

		Task<List<Team>> GetCompetitionTeams(CompetitionCode ccode, int? season, string stage = null);

		Task<List<Match>> GetCompetitionMatches(
			CompetitionCode ccode, int? season, MatchStatus? matchStatus = null,
			DateTime? dateFrom = null, DateTime? dateTo = null, int? matchDay = null,
			string group = null, string stage = null
		);
	}
}
