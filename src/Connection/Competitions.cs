/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Cielo.Core.Connection {
	using CompetitionsTuple = Tuple<CompetitionCode, Country, PayingPlan, string>;

	public static class Competitions {
		public static readonly List<CompetitionsTuple> COMPETITIONS_TABLE;

		static Competitions() {
			COMPETITIONS_TABLE = new List<CompetitionsTuple>();

			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.BSA,  item2: Country.Brazil,      item3: PayingPlan.TIER_ONE,
				item4: "Brasileirão e Série A"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.PL,   item2: Country.England,     item3: PayingPlan.TIER_ONE,
				item4: "Premier League"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.ELC,  item2: Country.England,     item3: PayingPlan.TIER_ONE,
				item4: "EFL Championship"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.CL,   item2: Country.Europe,      item3: PayingPlan.TIER_ONE,
				item4: "UEFA Champions League"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.EC,   item2: Country.Europe,      item3: PayingPlan.TIER_ONE,
				item4: "UEFA Euro"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.FL1,  item2: Country.France,      item3: PayingPlan.TIER_ONE,
				item4: "Ligue 1"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.BL1,  item2: Country.Germany,     item3: PayingPlan.TIER_ONE,
				item4: "Bundesliga"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.SA,   item2: Country.Italy,       item3: PayingPlan.TIER_ONE,
				item4: "Serie A"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.DED,  item2: Country.Netherlands, item3: PayingPlan.TIER_ONE,
				item4: "Eredivisie"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.PPL,  item2: Country.Portugal,    item3: PayingPlan.TIER_ONE,
				item4: "Primeira Liga"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.PD,   item2: Country.Spain,       item3: PayingPlan.TIER_ONE,
				item4: "LaLiga"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.WC,   item2: Country.World,       item3: PayingPlan.TIER_ONE,
				item4: "FIFA World Cup"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.BJL,  item2: Country.Belgium,     item3: PayingPlan.TIER_TWO,
				item4: "Jupiler Pro League"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.EL1,  item2: Country.England,     item3: PayingPlan.TIER_TWO,
				item4: "EFL League One"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.UEFA, item2: Country.Europe,      item3: PayingPlan.TIER_TWO,
				item4: "UEFA Europa League"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.DFB,  item2: Country.Germany,     item3: PayingPlan.TIER_TWO,
				item4: "German Cup"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.BL2,  item2: Country.Germany,     item3: PayingPlan.TIER_TWO,
				item4: "2. Bundesliga"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.SB,   item2: Country.Italy,       item3: PayingPlan.TIER_TWO,
				item4: "Serie B"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.SD,   item2: Country.Spain,       item3: PayingPlan.TIER_TWO,
				item4: "LaLiga 2"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.EL2,  item2: Country.England,     item3: PayingPlan.TIER_THREE,
				item4: "EFL League Two"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.FL2,  item2: Country.France,      item3: PayingPlan.TIER_THREE,
				item4: "Ligue 2"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.BL3,  item2: Country.Germany,     item3: PayingPlan.TIER_THREE,
				item4: "3. Bundesliga"
			));
			COMPETITIONS_TABLE.Add(new CompetitionsTuple(
				item1: CompetitionCode.NAT,  item2: Country.England,     item3: PayingPlan.TIER_FOUR,
				item4: "National League"
			));
		}

		public static Country GetCompetitionCountry(CompetitionCode cocode) {
			return COMPETITIONS_TABLE.Where(ce => ce.Item1 == cocode).Single().Item2;
		}

		public static List<CompetitionCode> GetCompetitions(Country c) {
			IEnumerable<CompetitionCode> req =
				from ce in COMPETITIONS_TABLE
				where ce.Item2 == c
				select ce.Item1;
			return req == null ? new List<CompetitionCode>() : req.ToList();
		}

		public static List<CompetitionCode> GetCompetitions(PayingPlan tier) {
			IEnumerable<CompetitionCode> req =
				from ce in COMPETITIONS_TABLE
				where ce.Item3 <= tier
				select ce.Item1;
			return req == null ? new List<CompetitionCode>() : req.ToList();
		}

		public static string getChampionshipName(CompetitionCode cocode) {
			return COMPETITIONS_TABLE.Where(ce => ce.Item1 == cocode).Single().Item4;
		}
	}
}
