/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

namespace Cielo.Core.Connection {
	/// @brief Base class for wrong replies
	public class WrongReply {
		public int HttpCode { get; protected set; }
		public string HttpReason { get; protected set; }
		public string ErrorMessage { get; protected set; }

		public WrongReply(int httpCode, string httpReason, string errorMessage) {
			HttpCode = httpCode;
			HttpReason = httpReason;
			ErrorMessage = errorMessage;
		}

		public override string ToString()
		{
			return string.Format("Error {0} ({1}): {2}", HttpCode, HttpReason, ErrorMessage);
		}
	}
}
