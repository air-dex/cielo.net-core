/// @section LICENSE
///
/// Copyright 2021 Romain Ducher
///
/// This file is part of Cielo.NET Core.
///
/// Cielo.NET Core is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// Cielo.NET Core is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
/// GNU Lesser General Public License for more details.
///
/// You should have received a copy of the GNU Lesser General Public License
/// along with Cielo.NET Core. If not, see <http://www.gnu.org/licenses/>.

namespace Cielo.Core.Connection {
	/// @brief WrongReply with additional data concerning excedeed quotas
	public class ExcedeedQuotaReply: WrongReply {
		/// @brief Number of seconds to wait in order to get non-429 answers.
		public uint CounterReset { get; protected set; }
		
		/// @brief Remaining requests for the following minute
		public uint RequestsAvailable { get; protected set; }

		public ExcedeedQuotaReply(
			uint counterReset, string errorMessage, uint requestsAvailable = 0,
			int httpCode = 429, string httpReason = "Too Many Requests"
		) :base(
			httpCode: httpCode,
			httpReason: httpReason,
			errorMessage: errorMessage
		) {
			CounterReset = counterReset;
			RequestsAvailable = requestsAvailable;
		}
	}
}
