# Cielo.NET Core

Backend library for Cielo.NET, containing connection to the football API and entities modeling championships.

Football data provided by the Football-Data.org API
